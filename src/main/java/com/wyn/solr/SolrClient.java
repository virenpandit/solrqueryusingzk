 package com.wyn.solr;

import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrDocument;

import java.util.Iterator;

public class SolrClient {

    public static void log(Object o) {
        System.out.println("" + o);
    }

    public static void main(String args[]) {
        new SolrClient().query();
    }

    public void query() {
        log("Inside: query().v.1.2");
        CloudSolrServer zkServer = null;
        try {
            zkServer = new CloudSolrServer("vsvphxupoc05.hotelgroup.com:9095");
            zkServer.connect();
            zkServer.setDefaultCollection("aemcontent");

            SolrQuery solrQuery = new SolrQuery();

            //solrQuery.set("q", "Notification");
            solrQuery.addFilterQuery("brand:'Microtel'");
            //solrQuery.set("defType", "edismax");
            // solrQuery.set("start", 0);
            solrQuery.set("rows", 10);
            //solrQuery.set("qf", "name^10.0 description^5.0");
            //solrQuery.addSortField("name_sort", SolrQuery.ORDER.asc);

            SolrParams params = SolrParams.toSolrParams(solrQuery.toNamedList());
            QueryResponse response = zkServer.query(params);

            SolrDocumentList docs = response.getResults();

            if(null!=docs) {
                Iterator iterator = docs.iterator();
                int i=0;
                while(null!=iterator && iterator.hasNext()) {
                    SolrDocument doc = (SolrDocument) iterator.next();
                    i++;
                }
                log("Total records=" + i);
            }

            zkServer.shutdown();

        } catch(Exception ex) {
            ex.printStackTrace();
            try {
                zkServer.shutdown();
            } catch(Exception ex2) { }
        }
        log("Exiting: query().v.1.2");
    }
}
